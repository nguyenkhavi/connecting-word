import authRoutes from "../modules/auth/authRoutes";
import { authJwt } from "../modules/auth/authControllers";
import gameRoutes from "../modules/game/gameRoutes";
const useRoutes = (app) => {
    app.use("/v1/auth", authRoutes);
    app.use("/v1/games", gameRoutes);
};

export default useRoutes;
