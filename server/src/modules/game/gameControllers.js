import gameServices from "./gameServices";
export const createGame = async (req, res) => {
    try {
        const userId = req.user || "";
        const data = await gameServices.createGame({ userId });
        res.json({ data });
    } catch (error) {
        res.sendStatus(500);
    }
};

export const createAnswerByGameId = async (req, res) => {
    try {
        const { id } = req.params;
        const { word } = req.body;
        const userId = req.user || "";
        const data = await gameServices.createAnswerByGameId({
            userId,
            gameId: id,
            word,
        });
        res.json({ data });
    } catch (error) {
        console.log({ error });

        res.sendStatus(500);
    }
};

export const getLatestAnswer = async (req, res) => {
    try {
        const { id } = req.params;
        const data = await gameServices.getLatestAnswer({ gameId: id });
        res.json({ data });
    } catch (error) {
        res.sendStatus(500);
    }
};
