import express from "express";
import * as controllers from "./gameControllers";
const gameRoutes = new express.Router();

gameRoutes.post("/", controllers.createGame);
gameRoutes.post("/:id/answers", controllers.createAnswerByGameId);
gameRoutes.get("/:id/answers/latest", controllers.getLatestAnswer);
export default gameRoutes;
