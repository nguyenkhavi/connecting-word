import gameModel from "../../models/game/gameModel";
import answerModel from "../../models/answer/answerModel";
const gameServices = {
    createGame({ userId, type }) {
        return gameModel.create({ userId, type });
    },
    createAnswerByGameId({ gameId, word, userId }) {
        return answerModel.create({ gameId, word, userId });
    },
    getLatestAnswer({ gameId }) {
        return answerModel.findOne({ gameId }).limit(1).sort({ $natural: -1 });
    },
};

export default gameServices;
