import mongoose from "mongoose";
const { Schema } = mongoose;

const schema = new Schema(
    {
        gameId: { type: Schema.Types.String, require: true },
        word: { type: Schema.Types.String, require: true },
        userId: { type: Schema.Types.String, require: true },
    },
    { timestamps: true }
);

export default mongoose.model("answer", schema);
