import mongoose from "mongoose";
const { Schema } = mongoose;
import { GAME_TYPE } from "../../constants";
const schema = new Schema(
    {
        userId: { type: Schema.Types.String, require: true },

        type: {
            type: Schema.Types.String,
            require: true,
            default: GAME_TYPE.COMPUTER,
        },
    },
    { timestamps: true }
);

export default mongoose.model("games", schema);
