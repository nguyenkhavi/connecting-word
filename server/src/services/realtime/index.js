import { Server } from "socket.io";
const sockets = {};
let io = null;
export const connect = (server) => {
    io = new Server(server, {
        cors: {
            origin: "*",
        },
    });
    io.on("connection", (socket) => {
        console.log("Socket connection", socket.id);
        sockets[socket.handshake.query.id] = socket.id;

        socket.on("disconnecting", async () => {
            for (const room of socket.rooms) {
                const payload = await getSocketsInRoom(room);
                const data = { type: "leave", payload };
                io.to(room).emit("data", { room, data });
            }
            delete sockets[socket.handshake.query.id];
            console.log("Disconnect", socket.id);
        });

        socket.on("subscribe", async ({ room, type }, callback) => {
            socket.join(room);
            const payload = await getSocketsInRoom(room);
            const data = { type, payload };
            if (typeof callback === "function") callback(data);
            socket.to(room).emit("data", { room, data });
            console.log("Joined room", room);
        });

        socket.on("unsubscribe", async ({ room, type }, callback) => {
            if (!room) {
                if (typeof callback === "function")
                    callback("Room name is required.");
            } else {
                socket.leave(room);
                const payload = await getSocketsInRoom(room);
                const data = { type, payload };
                io.to(room).emit("data", { room, data });
                console.log("Leaving room", room);
            }
        });

        socket.on("send", async ({ room, type, payload = {} }, callback) => {
            const data = { type, payload };

            if (room) {
                console.log(await getSocketsInRoom(room));

                socket.to(room).emit("data", { room, data });
                if (typeof callback === "function") callback(data);
            }
        });
    });
};
export const getSocketById = (user_id) => {
    return io.sockets.sockets.get(sockets[user_id]);
};
export const getSocketsInRoom = async (roomName) => {
    return await io
        .in(roomName)
        .fetchSockets()
        .then((_sockets) => {
            return _sockets.reduce((p, c) => {
                p[c.handshake.query.id] = c.id;
                return p;
            }, {});
        });
};
export const send = (room, data) => {
    io.to(room).emit("data", { room, data });
};
