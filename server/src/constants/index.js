export const STATUS = {
    PENDING: "pending",
    REJECTED: "rejected",
    ACCEPTED: "accepted",
};

export const GAME_TYPE = {
    COMPUTER: "computer",
    FRIEND: "friend",
};
