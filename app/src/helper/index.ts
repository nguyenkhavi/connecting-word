import {Image} from 'react-native';
import {IDimensions} from 'interfaces/common/IDimensions';
export const getImageSize: (uri: string) => Promise<IDimensions> = (
    uri: string,
) =>
    new Promise((resolve, reject) => {
        Image.getSize(
            uri,
            (width, height) => {
                resolve({width, height});
            },
            error => reject(error),
        );
    });
