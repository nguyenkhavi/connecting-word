import React, {useEffect, useRef, useState, memo} from 'react';
import {
    Animated,
    StyleProp,
    StyleSheet,
    View,
    ViewStyle,
    TextStyle,
    Text,
} from 'react-native';

export type IProps = {
    startPoint?: number;
    delay?: number;
    duration?: number;
    containerStyle?: StyleProp<ViewStyle>;
    barStyle?: StyleProp<ViewStyle>;
    percentageStyle?: StyleProp<TextStyle>;
};

const ProgressBar: React.FC<IProps> = (props: IProps) => {
    const barWidthAnim = useRef(new Animated.Value(0)).current;
    const [state, setState] = useState({
        percentage: props.startPoint
            ? Math.min(Math.max(props.startPoint, 0), 100)
            : 0,
    });
    useEffect(() => {
        barWidthAnim.addListener(progress => {
            //Avoid unnecessary rendering
            if (Math.round(progress.value) !== state.percentage) {
                setState({percentage: Math.round(progress.value)});
            }
            // setState({percentage: Math.round(progress.value)});
        });
        Animated.timing(barWidthAnim, {
            toValue: 100,
            delay: props.delay || 300,
            duration: props.duration || 3000,
            useNativeDriver: false,
        }).start();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [barWidthAnim]);
    return (
        <View style={[styles.container, props.containerStyle]}>
            <Animated.View
                style={[
                    styles.bar,
                    props.barStyle,
                    {
                        width: barWidthAnim.interpolate({
                            inputRange: [0, 100],
                            outputRange: ['0%', '100%'],
                        }),
                    },
                ]}
            />
            <Text
                style={[
                    styles.percentage,
                    props.percentageStyle,
                ]}>{`${state.percentage}%`}</Text>
        </View>
    );
};

export default memo(ProgressBar);

const BORDER_RADIUS = 4;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#363636',
        borderRadius: BORDER_RADIUS,
        borderWidth: 6,
        borderLeftWidth: 24,
        borderRightWidth: 24,
        borderColor: '#111111',
        flexDirection: 'row',
        // justifyContent: 'center',
        alignItems: 'center',
        height: 40,
        overflow: 'hidden',
        width: '100%',
    },
    bar: {
        borderRadius: BORDER_RADIUS,
        height: '100%',
        backgroundColor: '#71d201',
        width: '5%',
    },
    percentage: {
        marginLeft: 4,
        color: '#fff',
    },
});
