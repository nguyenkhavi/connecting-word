import React, {useRef, useState, memo} from 'react';
import {
    View,
    Text,
    FlatList,
    Animated,
    StyleSheet,
    Dimensions,
    NativeSyntheticEvent,
} from 'react-native';
import Media, {IMedia} from 'components/Media';

export type IProps = {
    list: IMedia[];
    init: number;
};

type IState = {
    page: number;
};

const ImageViewer: React.FC<IProps> = ({list = [], init = 0}: IProps) => {
    const listRef = useRef<FlatList | null>(null);
    const initIndex = Math.max(Math.min(init - 1, list.length), 0);
    const [state, setState] = useState<IState>({
        page: initIndex,
    });
    const scrollX = useRef(new Animated.Value(0)).current;

    const handleMomentumScrollEnd = (e: NativeSyntheticEvent<any>) => {
        if (!e) {
            return;
        }
        const xOffset = e.nativeEvent.contentOffset.x;
        const newPage = Math.ceil(xOffset / width);
        setState(s => ({...s, page: newPage}));
    };

    const genKey = (item: IMedia, index: number) => `${index}_${item.src}`;

    const renderItem = ({item}: {item: IMedia}) => {
        return <Media data={item} />;
    };
    const getItemLayout = (_: any, index: number) => ({
        length: width,
        offset: width * index,
        index,
    });

    return (
        <>
            <View style={styles.container}>
                <FlatList
                    ref={listRef}
                    data={list}
                    renderItem={renderItem}
                    extraData={state.page}
                    horizontal={true}
                    pagingEnabled={true}
                    keyExtractor={genKey}
                    onMomentumScrollEnd={handleMomentumScrollEnd}
                    onScroll={Animated.event(
                        [{nativeEvent: {contentOffset: {x: scrollX}}}],
                        {useNativeDriver: false},
                    )}
                    getItemLayout={getItemLayout}
                    initialScrollIndex={initIndex}
                    showsHorizontalScrollIndicator={false}
                />
            </View>
            <View style={styles.title}>
                <Text style={styles.textNum}>{`${state.page + 1}/${
                    list.length
                }`}</Text>
            </View>
            {/* <View style={styles.back}>
                <TouchableOpacity style={styles.icon}>
                    <Icon type="AntDesign" name="close" color={'#FFF'} />
                </TouchableOpacity>
            </View> */}
        </>
    );
};
const areEqual: (p: IProps, n: IProps) => boolean = (p: IProps, n: IProps) => {
    return JSON.stringify(p) === JSON.stringify(n);
};
export default memo(ImageViewer, areEqual);

const {width} = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#000',
    },
    back: {position: 'absolute', right: 0, top: 0},
    icon: {
        width: 50,
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    textNum: {color: '#FFF', fontSize: 14, fontWeight: 'bold'},
    title: {
        position: 'absolute',
        left: 0,
        top: 12,
        right: 0,
        alignItems: 'center',
        height: 50,
    },
});
