import React from 'react';
import {
    Text,
    TextStyle,
    TouchableOpacity,
    StyleSheet,
    ViewStyle,
    ActivityIndicator,
    ActivityIndicatorProps,
} from 'react-native';
import theme from 'theme';

interface IProps {
    onPress?: () => void;
    title: string;
    titleStyle?: TextStyle;
    isLoading?: boolean;
    containerStyle?: ViewStyle;
    indicatorProps?: ActivityIndicatorProps;
}

const CTButton = (props: IProps) => {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={[styles.container, props.containerStyle]}>
            {props.isLoading ? (
                <ActivityIndicator
                    color={theme.colors.white}
                    {...props.indicatorProps}
                />
            ) : (
                <Text style={[styles.text, props.titleStyle]}>
                    {props.title}
                </Text>
            )}
        </TouchableOpacity>
    );
};

export default CTButton;

const styles = StyleSheet.create({
    container: {
        elevation: 8,
        backgroundColor: '#009688',
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 12,
    },
    text: {
        fontSize: 18,
        color: '#fff',
        fontWeight: 'bold',
        alignSelf: 'center',
    },
});

CTButton.defaultProps = {
    isLoading: false,
};
