import React, {memo, useState, useEffect} from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    Image,
    StyleProp,
    ViewStyle,
} from 'react-native';
import {IMedia} from '..';
import {IDimensions} from 'interfaces/common/IDimensions';
import * as helper from '@helper';

const {width, height} = Dimensions.get('window');

type IState = {
    width?: number;
    height?: number;
    isLoading?: boolean;
};

type IProps = {
    media: IMedia;
    containerStyle?: StyleProp<ViewStyle>;
};

const Photos: React.FC<IProps> = ({media, containerStyle}: IProps) => {
    const [state, setState] = useState<IState>({
        width: 0,
        height: 0,
        isLoading: false,
    });

    useEffect(() => {
        getData();
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, []);

    const getData = async () => {
        try {
            const resp: IDimensions = await helper.getImageSize(media.src);
            const w = resp.width || 0;
            const h = resp.height || 0;
            if (w > 0 && h > 0) {
                const newHeight = (width * h) / w;
                setState({width: width, height: newHeight});
            } else {
                const newHeight = width * 0.5625;
                setState({width: width, height: newHeight});
            }
        } catch (error) {
            const newHeight = width * 0.5625;
            setState({width: width, height: newHeight});
        }
    };

    return (
        <View
            style={[
                styles.content,
                {height: height, width: width},
                containerStyle,
            ]}>
            <Image
                style={{width: state.width, height: state.height}}
                source={{uri: media.src}}
                resizeMode="stretch"
            />
        </View>
    );
};

const areEqual: (p: IProps, n: IProps) => boolean = (p: IProps, n: IProps) => {
    return JSON.stringify(p) === JSON.stringify(n);
};

export default memo(Photos, areEqual);

const WIDTH_IMG = width;
const HEIGHT_IMG = width * 0.5625;

const styles = StyleSheet.create({
    container: {flex: 1, backgroundColor: '#000'},
    back: {position: 'absolute', right: 0, top: 0},
    icon: {
        width: 50,
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignment: 'center',
    },
    video: {width: width, height: '100%', justifyContent: 'center'},
    posi: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        alignment: 'center',
    },
    content: {justifyContent: 'center', alignment: 'center'},
    textNum: {color: '#FFF', fontSize: 14},
    title: {
        position: 'absolute',
        left: 0,
        top: 12,
        right: 0,
        alignment: 'center',
        height: 50,
    },
    play: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        alignment: 'center',
    },
    posiImg: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        alignment: 'center',
        justifyContent: 'center',
    },
    img: {height: HEIGHT_IMG, width: WIDTH_IMG},
});
