import React, {memo, useState, useRef} from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    ActivityIndicator,
    TouchableOpacity,
    StyleProp,
    ViewStyle,
} from 'react-native';
import Video from 'react-native-video';
import {IMedia} from '..';
import Ionicon from 'react-native-vector-icons/Ionicons';
const {width} = Dimensions.get('window');

type IState = {
    status: 'paused' | 'ended' | 'playing' | 'loading';
};

type IProps = {
    media: IMedia;
    containerStyle?: StyleProp<ViewStyle>;
};

const CTVideo: React.FC<IProps> = ({media, containerStyle}: IProps) => {
    const videoRef = useRef<Video | null>(null);
    const [state, setState] = useState<IState>({
        status: 'paused',
    });

    const handlePause = () => {
        setState(s => ({
            ...s,
            status: s.status === 'playing' ? 'paused' : 'playing',
        }));
    };

    const handleReadyForDisplay = () => {
        console.log('ready');
        setState(s => ({...s, status: 'paused'}));
    };

    const handleEnd = () => {
        setState(s => ({...s, status: 'ended'}));
    };

    const handleReplay = () => {
        videoRef.current?.seek(0);
        setState(s => ({...s, status: 'playing'}));
    };

    const handleError = () => {
        setState(s => ({...s, status: 'paused'}));
    };

    const {status} = state;

    const icon = () => {
        switch (status) {
            case 'ended':
                return (
                    <TouchableOpacity
                        onPress={handleReplay}
                        style={styles.play}>
                        <Ionicon name="reload" color={'#F5F5F5'} size={40} />
                    </TouchableOpacity>
                );
            case 'loading':
                return (
                    <View style={styles.play}>
                        <ActivityIndicator size={'large'} color={'#F5F5F5'} />
                    </View>
                );
            case 'paused':
                return (
                    <View style={styles.play}>
                        <Ionicon name="ios-play" color={'#F5F5F5'} size={40} />
                    </View>
                );
            default:
                return <View />;
        }
    };
    return (
        <TouchableOpacity style={containerStyle} onPress={handlePause}>
            <View style={styles.video}>
                <Video
                    ref={videoRef}
                    onEnd={handleEnd}
                    onReadyForDisplay={handleReadyForDisplay}
                    source={{uri: media.src}}
                    paused={status === 'paused'}
                    resizeMode="contain"
                    style={styles.position}
                    onError={handleError}
                    repeat={false}
                    poster={media.thumbnail}
                />
            </View>
            {icon()}
        </TouchableOpacity>
    );
};

const areEqual: (p: IProps, n: IProps) => boolean = (p: IProps, n: IProps) => {
    return JSON.stringify(p) === JSON.stringify(n);
};

export default memo(CTVideo, areEqual);

const WIDTH_IMG = width;
const HEIGHT_IMG = width * 0.5625;

const styles = StyleSheet.create({
    container: {flex: 1, backgroundColor: '#000'},
    back: {position: 'absolute', right: 0, top: 0},
    icon: {
        width: 50,
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        alignment: 'center',
    },
    video: {width: width, height: '100%', justifyContent: 'center'},
    position: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    content: {justifyContent: 'center', alignment: 'center'},
    textNum: {color: '#FFF', fontSize: 14},
    title: {
        position: 'absolute',
        left: 0,
        top: 12,
        right: 0,
        alignment: 'center',
        height: 50,
    },
    play: {
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    positionImg: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        alignment: 'center',
        justifyContent: 'center',
    },
    img: {height: HEIGHT_IMG, width: WIDTH_IMG},
});
