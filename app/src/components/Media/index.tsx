import React, {memo} from 'react';
import {StyleProp, ViewStyle} from 'react-native';
import Photo from '../Media/components/Photo';
import Video from '../Media/components/Video';
const videoTypes = ['mp4', 'avi', 'mpeg4', 'mov', 'wmv', 'flv', '3gpp'];

export type IMedia = {
    thumbnail?: string;
    src: string;
    type:
        | 'mp4'
        | 'avi'
        | 'mpeg4'
        | 'mov'
        | 'wmv'
        | 'flv'
        | '3gpp'
        | 'gif'
        | 'jpg'
        | 'jpeg'
        | 'png'
        | 'webp';
};

type IProps = {
    data: IMedia;
    containerStyle?: StyleProp<ViewStyle>;
};

const Media = ({data, containerStyle}: IProps) => {
    const isVideo: boolean = videoTypes.includes(data.type);
    return isVideo ? (
        <Video media={data} containerStyle={containerStyle} />
    ) : (
        <Photo media={data} containerStyle={containerStyle} />
    );
};

const areEqual: (p: IProps, n: IProps) => boolean = (p: IProps, n: IProps) => {
    return JSON.stringify(p) === JSON.stringify(n);
};

export default memo(Media, areEqual);
