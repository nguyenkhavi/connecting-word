import React, {memo, useRef, useState} from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    PanResponder,
    Animated,
} from 'react-native';
import Media, {IMedia} from '@components/Media';
type IProps = {
    list: IMedia[];
};

const SwipeDeck = (props: IProps) => {
    const {width, height} = Dimensions.get('window');
    const animatedValue = useRef(new Animated.ValueXY()).current;
    const [index, setIndex] = useState(0);

    const handlePanResponderMove = (
        _: any,
        gesture: {dx: number; dy: number},
    ) => {
        animatedValue.setValue({x: gesture.dx, y: gesture.dy});
    };

    const handlePanResponderRelease = (_: any, gesture: {dx: number}) => {
        const callback = () => {
            animatedValue.setValue({x: 0, y: 0});
            setIndex(idx => idx + 1);
        };
        const dx = gesture.dx;
        if (dx < -0.25 * width) {
            Animated.spring(animatedValue, {
                toValue: {x: -width * 2, y: 0},
                useNativeDriver: false,
            }).start(callback);
        } else if (dx > -0.25 * width) {
            Animated.spring(animatedValue, {
                toValue: {x: width * 2, y: 0},
                useNativeDriver: false,
            }).start(callback);
        } else {
            Animated.timing(animatedValue, {
                toValue: {x: 0, y: 0},
                useNativeDriver: false,
            }).start();
        }
    };
    const panResponder = useRef(
        PanResponder.create({
            onStartShouldSetPanResponder: () => true,
            onPanResponderMove: handlePanResponderMove,
            onPanResponderRelease: handlePanResponderRelease,
        }),
    ).current;
    const renderItem = (item: IMedia, idx: number) => {
        if (idx < index) return <View key={JSON.stringify({item, idx})} />;
        if (idx === index) {
            return (
                <Animated.View
                    style={[
                        styles.card,
                        {
                            transform: [
                                {translateX: animatedValue.x},
                                {
                                    translateY: animatedValue.y.interpolate({
                                        inputRange: [
                                            -height * 0.035,
                                            +height * 0.035,
                                        ],
                                        outputRange: [
                                            -height * 0.035,
                                            +height * 0.035,
                                        ],
                                        extrapolate: 'clamp',
                                    }),
                                },
                                {
                                    rotate: animatedValue.x.interpolate({
                                        inputRange: [-width * 1.5, width * 1.5],
                                        outputRange: ['-120deg', '120deg'],
                                    }),
                                },
                            ],
                        },
                    ]}
                    key={JSON.stringify({item, idx})}>
                    <Media containerStyle={styles.media} data={item} />
                </Animated.View>
            );
        }
        return (
            <View style={styles.card} key={JSON.stringify({item, idx})}>
                <Media containerStyle={styles.media} data={item} />
            </View>
        );
    };
    return (
        <View style={styles.container} {...panResponder.panHandlers}>
            {props.list.map(renderItem).reverse()}
        </View>
    );
};

const areEqual: (p: IProps, n: IProps) => boolean = (p: IProps, n: IProps) => {
    return JSON.stringify(p) === JSON.stringify(n);
};
export default memo(SwipeDeck, areEqual);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    card: {
        position: 'absolute',
        flex: 1,
        width: '90%',
        height: '90%',
    },
    media: {
        width: '100%',
        height: '100%',
        overflow: 'hidden',
        borderRadius: 20,
    },
});
