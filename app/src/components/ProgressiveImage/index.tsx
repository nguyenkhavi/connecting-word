import React, {useRef} from 'react';
import {View, StyleSheet, Animated} from 'react-native';
import {IMedia} from 'components/Media';
type IProps = {
    source: IMedia;
    default: IMedia;
};
const ProgressiveImage = (props: IProps) => {
    const defaultImageAnimated = useRef(new Animated.Value(0)).current;
    const imageAnimated = useRef(new Animated.Value(0)).current;

    const handleDefaultImageLoad = () => {
        Animated.timing(defaultImageAnimated, {
            toValue: 1,
            useNativeDriver: true,
        }).start();
    };

    const handleImageLoad = () => {
        Animated.timing(imageAnimated, {
            toValue: 1,
            useNativeDriver: true,
        }).start();
    };

    return (
        <View style={styles.container}>
            <Animated.Image
                source={{uri: props.default.src}}
                style={[{opacity: defaultImageAnimated}]}
                onLoad={handleDefaultImageLoad}
                blurRadius={1}
            />
            <Animated.Image
                source={{uri: props.source.src}}
                style={[{opacity: imageAnimated}, styles.imageOverlay]}
                onLoad={handleImageLoad}
            />
        </View>
    );
};

export default ProgressiveImage;

const styles = StyleSheet.create({
    container: {
        backgroundColor: '#e1e4e8',
        height: 400,
    },
    imageOverlay: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
});
