import React, {memo} from 'react';
import {
    View,
    Text,
    TextStyle,
    TextInput,
    TextInputProps,
    StyleSheet,
    ViewStyle,
} from 'react-native';
import theme from 'theme';

interface IProps extends TextInputProps {
    label?: string;
    textLabelStyle?: TextStyle;
    errorMessage?: string;
    textErrorStyle?: TextStyle;
    containerStyle?: ViewStyle;
}

const CTInput = (props: IProps) => {
    return (
        <View style={[styles.container, props.containerStyle]}>
            {!!props.label && (
                <Text style={[styles.textLabel, props.textLabelStyle]}>
                    {props.label}
                </Text>
            )}
            <TextInput {...props} style={[styles.input, props.style]} />
            {!!props.errorMessage && (
                <Text style={[styles.textError, props.textErrorStyle]}>
                    {props.errorMessage}
                </Text>
            )}
        </View>
    );
};

export default memo(CTInput);

const styles = StyleSheet.create({
    container: {
        marginVertical: 8,
    },
    input: {
        borderWidth: 2,
        borderColor: theme.colors.gray,
        borderRadius: 4,
        paddingVertical: 4,
        paddingHorizontal: 8,
    },
    textLabel: {
        color: '#000',
        marginLeft: 8,
        marginBottom: 2,
    },
    textError: {
        color: theme.colors.red,
        marginRight: 8,
        marginTop: 2,
        textAlign: 'right',
    },
});
