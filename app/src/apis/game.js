import axios from 'axios';
const gameAPIs = {
    createGame() {
        return axios({
            url: 'v1/games',
            method: 'POST',
        })
            .then(res => res.data)
            .catch(e => {
                throw e.response;
            });
    },
    createAnswerByGameId(gameId, word) {
        return axios({
            url: `v1/games/${gameId}/answers`,
            method: 'POST',
            data: {
                word,
            },
        })
            .then(res => res.data)
            .catch(e => {
                throw e.response;
            });
    },
    getLatestAnswerByGameId(gameId) {
        return axios({
            url: `v1/games/${gameId}/answers/latest`,
            method: 'GET',
        })
            .then(res => res.data)
            .catch(e => {
                throw e.response;
            });
    },
};

export default gameAPIs;
