import io, {Socket} from 'socket.io-client';
interface ISendedDataProps {
    type: string;
    room: string;
    payload: any;
    callback: (data: any) => void;
}

interface IConnectProps {
    url: string;
    query: any;
    callback: (data: any) => void;
}

interface ISubscribeProps {
    room: string;
    type: string;
    fn?: (data: any) => void;
}

class RealtimeClient {
    handlers: {
        room: string;
        type: string;
        function?: (data: any) => void;
    }[];
    isConnected: boolean;
    socket: Socket;
    constructor() {
        this.handlers = [];
        this.isConnected = false;
        this.socket = io();
    }
    connect({url, query, callback}: IConnectProps) {
        this.socket = io(url, {query});
        this.socket.on('connect', () => {
            const id: string = this.socket.id;
            if (typeof callback === 'function') {
                callback(id);
            }
            this.handlers.forEach(handler => {
                this.socket.emit(
                    'subscribe',
                    {
                        room: handler.room,
                        type: handler.type,
                    },
                    (data: any) => {
                        if (typeof handler.function === 'function' && data) {
                            handler.function(data);
                        }
                    },
                );
            });
        });
        this.socket.on('connect_error', e => {
            if (typeof callback === 'function') {
                callback(e);
            }
            throw e;
        });

        this.socket.on('data', ({data, room}) => {
            this.handlers.forEach(handler => {
                if (handler.room === room) {
                    if (typeof handler.function === 'function') {
                        handler.function(data);
                    }
                }
            });
        });
    }
    subscribe({room, type, fn}: ISubscribeProps) {
        const handler = {room, type, function: fn};

        if (!this.socket) {
            if (typeof fn === 'function') {
                this.handlers.push(handler);
            }
        } else {
            this.socket.emit('subscribe', {room, type}, (data: any) => {
                if (typeof fn === 'function' && data) {
                    fn(data);
                }
                this.handlers.push(handler);
            });
        }
    }

    unsubscribe({room, type, fn}: ISubscribeProps) {
        if (!this.socket) {
            this.handlers = this.handlers.filter(handler => {
                if (typeof fn === 'function' && handler.function !== fn) {
                    return true;
                } else if (typeof fn === 'string' && handler.room !== fn) {
                    return true;
                }
                return false;
            });
        } else {
            this.socket.emit('unsubscribe', {room, type}, () => {
                this.handlers = this.handlers.filter(handler => {
                    if (typeof fn === 'function' && handler.function !== fn) {
                        return true;
                    } else if (typeof fn === 'string' && handler.room !== fn) {
                        return true;
                    }
                    return false;
                });
            });
        }
    }

    send({room, type, payload, callback}: ISendedDataProps) {
        this.socket.emit('send', {room, type, payload}, callback);
    }
}

const RealtimeService = new RealtimeClient();

export default RealtimeService;
