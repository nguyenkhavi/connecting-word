import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {IAuthParams} from './IAuthParams';
import Login from 'screens/Auth/Login';
import Register from 'screens/Auth/Register';
const Stack = createNativeStackNavigator<IAuthParams>();
const AuthRoute = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={'Login'}>
                <Stack.Screen name={'Login'} component={Login} />
                <Stack.Screen name={'Register'} component={Register} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default AuthRoute;
