import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {IParams} from './IParams';
import GameDetail from 'screens/Game/Detail';

const Stack = createNativeStackNavigator<IParams>();
const AppNavigator = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={'GameDetail'}>
                <Stack.Screen name={'GameDetail'} component={GameDetail} />
            </Stack.Navigator>
        </NavigationContainer>
    );
};

export default AppNavigator;
