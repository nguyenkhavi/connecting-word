import React, {memo, useState} from 'react';

import {StyleSheet, Text, View, Alert} from 'react-native';
import Input from 'components/Input';
import Button from 'components/Button';
import authAPIs from 'apis/auth';

interface IState {
    username: string;
    password: string;
    confirm: string;
    email: string;
    isLoading: boolean;
}
const Register = () => {
    const [state, setState] = useState<IState>({
        username: '',
        password: '',
        email: '',
        confirm: '',
        isLoading: false,
    });

    const handleTextChange = (name: string) => (value: string) => {
        setState(s => ({
            ...s,
            [name]: value,
        }));
    };

    const handleRegister = async () => {
        console.log({state});

        setState(s => ({
            ...s,
            isLoading: true,
        }));
        try {
            const {accessToken, refreshToken, user} = await authAPIs.signUp({
                username: state.username,
                password: state.password,
                email: state.email,
            });
        } catch (error) {
            const message = error.data.message || '';
            console.log({message});
        } finally {
            setState(s => ({
                ...s,
                isLoading: false,
            }));
        }
    };
    return (
        <View style={styles.container}>
            <Input
                value={state.username}
                label="Username"
                onChangeText={handleTextChange('username')}
            />
            <Input
                value={state.email}
                label="Email"
                onChangeText={handleTextChange('email')}
            />
            <Input
                value={state.password}
                label="Password"
                onChangeText={handleTextChange('password')}
            />
            <Input
                value={state.confirm}
                label="Confirm"
                onChangeText={handleTextChange('confirm')}
            />

            <Button
                onPress={handleRegister}
                isLoading={state.isLoading}
                title="Register"
            />
        </View>
    );
};

export default memo(Register);
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        padding: 16,
    },
});
