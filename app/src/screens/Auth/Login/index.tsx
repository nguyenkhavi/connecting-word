import React, {useState} from 'react';

import {StyleSheet, Text, View, TextInput, Alert} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import constants from 'constants/index';
import {
    GoogleSignin,
    statusCodes,
} from '@react-native-google-signin/google-signin';
import Input from 'components/Input';
import Button from 'components/Button';
import authAPIs from 'apis/auth';
import {useNavigation} from '@react-navigation/native';
import {IError} from 'interfaces/common/IError';

interface IState {
    username: string;
    password: string;
    isLoading: boolean;
}
const Login = () => {
    const navigation = useNavigation();
    const [state, setState] = useState<IState>({
        username: '',
        password: '',
        isLoading: false,
    });

    React.useEffect(() => {
        GoogleSignin.configure({
            webClientId: constants.GOOG_API_KEY,
        });
    }, []);
    const loginGoogle = async () => {
        try {
            await GoogleSignin.hasPlayServices();
            await GoogleSignin.signIn();
            const {accessToken} = await GoogleSignin.getTokens();
            const res = await authAPIs
                .logInWithGoogle(accessToken)
                .then(r => r.data);
            // await AsyncStorage.setItem('@token', res.token);
            // CodePush.restartApp();
        } catch (error) {
            console.log(JSON.stringify(error));

            // if (error.code === statusCodes.SIGN_IN_CANCELLED) {
            //     // user cancelled the login flow
            // } else if (error.code === statusCodes.IN_PROGRESS) {
            //     // operation (e.g. sign in) is in progress already
            //     Alert.alert('error', 'error.google_login', [
            //         {text: 'close', style: 'cancel'},
            //     ]);
            // } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
            //     // play services not available or outdated
            //     Alert.alert('error', 'error.google_login', [
            //         {text: 'close', style: 'cancel'},
            //     ]);
            // } else {
            //     Alert.alert('error', 'error.google_login', [
            //         {text: 'close', style: 'cancel'},
            //     ]);
            // }
        }
    };
    const handleLogIn = async () => {
        setState(s => ({
            ...s,
            isLoading: true,
        }));
        try {
            const {accessToken, refreshToken, user} = await authAPIs.logIn({
                username: state.username,
                password: state.password,
            });
            console.log({accessToken, refreshToken, user});
        } catch (error: IError) {
            const message = error.data.message || '';
            console.log({message});
        } finally {
            setState(s => ({
                ...s,
                isLoading: false,
            }));
        }
    };
    const handleRegister = () => {
        navigation.navigate({key: 'Register'});
    };
    const handleTextChange = (name: string) => (value: string) => {
        setState(s => ({
            ...s,
            [name]: value,
        }));
    };
    return (
        <View style={styles.container}>
            {/* <Button onPress={loginGoogle} title="Login with google" /> */}
            <Input
                value={state.username}
                label="Username"
                onChangeText={handleTextChange('username')}
            />
            <Input
                value={state.password}
                label="Password"
                onChangeText={handleTextChange('password')}
            />
            <Button onPress={handleLogIn} title="Log in" />
            <Text style={styles.register} onPress={handleRegister}>
                Sign up
            </Text>
        </View>
    );
};

export default Login;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        padding: 16,
    },
    register: {
        textDecorationLine: 'underline',
        fontWeight: 'bold',
        margin: 8,
    },
});
