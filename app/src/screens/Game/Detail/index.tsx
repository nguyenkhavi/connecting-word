import {StyleSheet, Text, View, TextInput, Button} from 'react-native';
import React from 'react';
import gameAPIs from 'apis/game';
import RealtimeService from 'services/realtime';
export default function GameDetail() {
    const gameId = '61f4b0f208470174d60e7b6a';
    const [state, setState] = React.useState({
        isLoading: true,
        latestAnswer: {},
        users: [],
        word: '',
    });

    React.useEffect(() => {
        getLatestAnswer();
        RealtimeService.subscribe({
            room: gameId,
            type: 'join',
            fn: ({type, payload}) => {
                console.log({type, payload});
            },
        });
        return () => RealtimeService.unsubscribe({room: gameId, type: 'leave'});
    }, []);

    const getLatestAnswer = async () => {
        const {data} = await gameAPIs.getLatestAnswerByGameId(gameId);
        setState(s => ({
            ...s,
            latestAnswer: data,
        }));
    };

    const handleSubmit = async () => {
        await gameAPIs.createAnswerByGameId(gameId, state.word);
        RealtimeService.send({
            room: gameId,
            payload: {word: state.word},
            type: 'new_word',
        });
        await getLatestAnswer();
    };

    const handleTextChange = text => {
        setState(s => ({
            ...s,
            word: text,
        }));
    };
    return (
        <View style={styles.container}>
            <Text>{state.latestAnswer?.word || 'Chuẩn bị'}</Text>
            <TextInput
                value={state.word}
                placeholder="Nhập từ"
                onChangeText={handleTextChange}
            />
            <Button onPress={handleSubmit} title="Gửi" />
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
});
