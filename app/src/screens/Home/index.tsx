import React from 'react';

import {StyleSheet, View} from 'react-native';
import Button from 'components/Button';
import {useNavigation} from '@react-navigation/native';

interface IState {
    username: string;
    password: string;
    isLoading: boolean;
}
const Home = () => {
    const navigation = useNavigation();

    const handlePlayWithComputer = () => {};

    return (
        <View style={styles.container}>
            <Button
                onPress={handlePlayWithComputer}
                title="Play with computer"
            />
            <Button onPress={handlePlayWithComputer} title="Play with friend" />
        </View>
    );
};

export default Home;
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#FFF',
        padding: 16,
    },
    register: {
        textDecorationLine: 'underline',
        fontWeight: 'bold',
        margin: 8,
    },
});
