import React, {useContext, createContext} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import axios from 'axios';
import constants from 'constants/index';
import RealtimeService from 'services/realtime';
import authAPIs from 'apis/auth';

export const Context = createContext<any[]>([]);

axios.defaults.baseURL = constants.API_URL;

interface IProps {
    children: React.ReactNode;
    value?: any | null;
}
interface IState {
    loading: boolean;
    user: any | null;
    accessToken: string | null;
}
export const Provider = ({children, value}: IProps) => {
    const [context, setContext] = React.useState<IState>(value);
    const connected = React.useRef(false);

    const auth = async () => {
        const token: string | '' =
            (await AsyncStorage.getItem('@access_token_flm')) || '';

        if (token) {
            axios.defaults.headers.common.Authorization = `Bearer ${token}`;
            setContext(s => ({
                ...s,
                loading: false,
                accessToken: token,
            }));

            authAPIs.check().then(res => {
                const {accessToken, user} = res;
                AsyncStorage.setItem('@access_token_flm', res.accessToken);
                axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
                setContext(s => ({
                    ...s,
                    loading: false,
                    user: user,
                }));
            });
        }
    };
    React.useEffect(() => {
        auth();
    }, []);

    React.useEffect(() => {
        if (context.user && !connected.current) {
            try {
                const id = context.user.id || 'user-id';

                RealtimeService.connect({
                    url: constants.API_URL,
                    query: {id},
                    callback: res => {
                        console.log('socket-io connected', res);
                    },
                });
            } catch (e) {
                console.log('socket-io error', e);
            }
        }
    }, [context.user]);

    const values = React.useMemo(() => [context, setContext], [context]);

    return <Context.Provider value={values}>{children}</Context.Provider>;
};

export function useAppContext() {
    const context = useContext(Context);

    if (!context) {
        console.error('Error deploying Context!!!');
    }

    return context;
}
