module.exports = {
    presets: ['module:metro-react-native-babel-preset'],
    plugins: [
        [
            'module-resolver',
            {
                root: ['./src'],
                extensions: [
                    '.ios.js',
                    '.android.js',
                    '.js',
                    '.jsx',
                    '.ts',
                    '.tsx',
                    '.json',
                ],
                alias: {
                    tests: ['./tests/'],
                    '@components': './src/components',
                    '@helper': './src/helper',
                    '@interfaces': './src/interfaces',
                    '@services': './src/services',
                },
            },
        ],
    ],
};
