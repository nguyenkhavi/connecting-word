import React from 'react';
import AppNavigator from 'routes/AppRoutes';
import {Provider, useAppContext} from 'context';
import AuthRoute from 'routes/AuthRoute';

import gameAPIs from 'apis/game';
const App = () => {
    return (
        <Provider value={{loading: true, accessToken: '', user: {}}}>
            <MainNavigation />
        </Provider>
    );
};

const MainNavigation = () => {
    const context = useAppContext();
    console.log(context);
    // return <AppNavigator />;

    return context?.user?._id ? <AppNavigator /> : <AuthRoute />;
};

export default App;
